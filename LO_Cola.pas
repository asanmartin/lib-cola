unit LO_Cola;

interface

	const
		_BASE_NAME = ''; // puede ser vacio
		_BASE_ROOT = ''; //ruta por defecto es en donde se encuentra el exe
		_NOMBRE_CONTROL = _BASE_NAME + '.CON';
		_NOMBRE_DATOS = _BASE_NAME + '.DAT';
		_POSICION_NULA = -1;
		_FIRST_ELEMENT = 0;
		_CLAVE_NULA = '';
	//end const's
	type
		tClave = String[6];
		tPosicion = Longint;
		tEnlace = Longint;

		tRegistroDatos = record
				clave: tClave; //
				nombre: String[10]; 
				enlace: tEnlace; //posición física del siguiente elemento
		end;
		tArchivoDatos = file of tRegistroDatos;
		
		tRegistroControl = record
				primero, ultimo :tEnlace; // indicando las posiciones físicas de la primera y la última carta del mazo
				borrado: tEnlace;//enlaza a todas las cartas “retiradas” del mazo
				total: byte;
		end;
		tArchivoControl = file of tRegistroControl;
		
		tCola = record
				c: tArchivoControl;
				d: tArchivoDatos;
		end;
	//end-type

	{var
		Cola:tCola;}

	Procedure LO_CrearCola(var Cola:tCola; nombre,ruta:String);
	Procedure LO_AbrirCola(var Cola:tCola);
	Procedure LO_CerrarCola(var Cola:tCola);
	Procedure LO_DestruirCola(var Cola:tCola; nombre, ruta:String);
	Function LO_ColaVacia(var Cola:tCola):Boolean;
	Procedure LO_Frente(var Cola:tCola; var Reg:tRegistroDatos); // Seleciona el primer elemento
	Procedure LO_Decolar(var Cola:tCola); // Elimina el primer elemento
	Procedure LO_Encolar(var Cola:tCola; Reg:tRegistroDatos); // Agrega en el ultimo lugar el elemento
	Function LO_PosNula(var Cola:tCola):tPosicion;
	Function LO_TotalEnCola(var Cola:tCola):integer; // devuelve la cantidad de elementos en el metodo

implementation
	uses SysUtils, Dialogs;

	Procedure LO_CrearCola(var Cola:tCola; nombre,ruta:String);
		var
			bExtCon,bExtDat:Boolean; RC:tRegistroControl;
		begin
			Assign (Cola.c, ruta + nombre + '.CON');
			Assign (Cola.d, ruta + nombre + '.DAT');
			bExtCon := fileexists(ruta + nombre + '.CON');
			bExtDat := fileexists(ruta + nombre + '.DAT');
			
			if not(bExtCon) or not(bExtDat) then begin
				RC.primero := _POSICION_NULA;
				RC.ultimo := _POSICION_NULA;
				RC.borrado := _POSICION_NULA;
				RC.total := 0;
				Rewrite(Cola.c);
				Write(Cola.c, RC);
				Rewrite(Cola.d);
				Close(Cola.c);
				Close(Cola.d);
			end;//end-if
		end;
	//end LO_CrearCola
	Procedure LO_AbrirCola(var Cola:tCola);
		begin
			Reset(Cola.c);
			Reset(Cola.d);
		end;
	//end LO_AbrirCola 
	Procedure LO_CerrarCola(var Cola:tCola);
		begin
			Close(Cola.c);
			Close(Cola.d);
		end;
	//end LO_CerrarCola
	Procedure LO_DestruirCola(var Cola:tCola; nombre, ruta:String);
		begin
			Assignfile(Cola.c, ruta + nombre + '.CON');
			Assignfile(Cola.d, ruta + nombre + '.DAT');
			deletefile(ruta + nombre + '.CON');
			deletefile(ruta + nombre + '.DAT');
		end;
	//end LO_DestruirCola
	Function LO_ColaVacia(var Cola:tCola):Boolean;
		var
			RC:tRegistroControl;
		begin
			Seek(Cola.c, 0);
			Read(Cola.c, RC);
			LO_ColaVacia:= (RC.primero = _POSICION_NULA);
		end;
	//end LO_ColaVacia
	Procedure LO_Frente(var Cola:tCola; var Reg:tRegistroDatos);
		var
			RC:tRegistroControl;
		begin
			Seek(Cola.c, 0);
			Read(Cola.c, RC);
			Seek(Cola.d, RC.primero);
			Read(Cola.d, Reg);
		end;
	//end LO_Frente
	Procedure LO_Decolar(var Cola:tCola);
		var
			RC:tRegistroControl; pos:tPosicion; Reg:tRegistroDatos;
		begin
			Seek(Cola.c, 0);
			Read(Cola.c, RC);
			pos := RC.primero;
			Seek(Cola.d, pos);
			Read(Cola.d, Reg);
			RC.primero:= Reg.enlace;
			
			if (Reg.enlace = _POSICION_NULA) then RC.ultimo := _POSICION_NULA;
			
			Reg.enlace := RC.borrado;
			RC.borrado := pos;
			RC.total := RC.total -1;
			Seek(Cola.c, 0);
			Write(Cola.c, RC);
			Seek(Cola.d, pos);
			Write(Cola.d, Reg);
		end;
	//end LO_Decolar
	Procedure LO_Encolar(var Cola:tCola; Reg:tRegistroDatos);
		var
			RC:tRegistroControl; pos:tPosicion; RAux:tRegistroDatos;
		begin
			Seek(Cola.c, 0);
			Read(Cola.c, RC);
			
			if (RC.borrado <> _POSICION_NULA) then begin
				pos := RC.borrado;
				Seek(Cola.d, pos);
				Read(Cola.d, RAux);
				RC.borrado := RAux.enlace;
			end else
				pos:= filesize(Cola.d)
			;//end-if
			
			if (RC.primero = _POSICION_NULA) then begin
				Reg.enlace := _POSICION_NULA;
				RC.primero := pos;
				RC.ultimo := pos;
			end	else begin
				Seek(Cola.d, RC.ultimo);
				Read(Cola.d, RAux);
				RAux.enlace := pos;
				Seek(Cola.d, RC.ultimo);
				Write(Cola.d, RAux);
				RC.ultimo := pos;
				Reg.enlace := _POSICION_NULA;
			end;
			
			RC.total := RC.total +1;
			Seek(Cola.c, 0);
			Write(Cola.c, RC);
			Seek(Cola.d, pos);
			Write(Cola.d, Reg);
		end;
	//end LO_Encolar
	Function LO_PosNula(var Cola:tCola):tPosicion;
		begin
			LO_PosNula := _POSICION_NULA;
		end;
	//end LO_PosNula
	Function LO_TotalEnCola(var Cola:tCola):integer;
		var
			RC:tRegistroControl;
		begin
			Seek(Cola.c, 0);
			Read(Cola.c, RC);
			LO_TotalEnCola := RC.total;
		end;
	//end LO_TotalEnCola
end.

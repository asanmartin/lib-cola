object Form1: TForm1
  Left = 369
  Top = 162
  Width = 870
  Height = 427
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -16
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 19
  object lst1: TListBox
    Left = 64
    Top = 64
    Width = 209
    Height = 249
    ItemHeight = 19
    TabOrder = 0
  end
  object btnCrear: TButton
    Left = 304
    Top = 104
    Width = 137
    Height = 33
    Caption = 'Crear Structura'
    TabOrder = 1
    OnClick = btnCrearClick
  end
  object btnCheck: TButton
    Left = 304
    Top = 64
    Width = 137
    Height = 33
    Caption = 'Checkear Structura'
    TabOrder = 2
    OnClick = btnCheckClick
  end
  object grp1: TGroupBox
    Left = 528
    Top = 32
    Width = 233
    Height = 105
    Caption = 'grp1'
    TabOrder = 3
    object lblNombre: TLabel
      Left = 16
      Top = 40
      Width = 57
      Height = 19
      Caption = 'Nombre'
    end
    object lblRuta: TLabel
      Left = 40
      Top = 64
      Width = 32
      Height = 19
      Caption = 'Ruta'
    end
    object edtNombre: TEdit
      Left = 96
      Top = 32
      Width = 121
      Height = 27
      TabOrder = 0
      Text = 'super'
    end
    object edtRuta: TEdit
      Left = 96
      Top = 62
      Width = 121
      Height = 27
      TabOrder = 1
    end
  end
  object btnEliminar: TButton
    Left = 304
    Top = 144
    Width = 137
    Height = 33
    Caption = 'Eliminar Structure'
    TabOrder = 4
    OnClick = btnEliminarClick
  end
  object btnClear: TButton
    Left = 200
    Top = 40
    Width = 65
    Height = 17
    Caption = 'Clear'
    TabOrder = 5
    OnClick = btnClearClick
  end
  object btnAlta: TButton
    Left = 304
    Top = 224
    Width = 137
    Height = 33
    Caption = '+ Elemento'
    TabOrder = 6
    OnClick = btnAltaClick
  end
  object grp2: TGroupBox
    Left = 488
    Top = 168
    Width = 281
    Height = 153
    Caption = 'grp2'
    TabOrder = 7
    object lblClave: TLabel
      Left = 24
      Top = 40
      Width = 38
      Height = 19
      Caption = 'Clave'
    end
    object lblName: TLabel
      Left = 24
      Top = 72
      Width = 41
      Height = 19
      Caption = 'Name'
    end
    object edtClave: TEdit
      Left = 120
      Top = 40
      Width = 121
      Height = 27
      TabOrder = 0
    end
    object edtName: TEdit
      Left = 120
      Top = 72
      Width = 121
      Height = 27
      TabOrder = 1
    end
  end
  object btnListado: TButton
    Left = 304
    Top = 264
    Width = 137
    Height = 33
    Caption = 'Listado'
    TabOrder = 8
    OnClick = btnListadoClick
  end
  object btnBaja: TButton
    Left = 304
    Top = 304
    Width = 137
    Height = 33
    Caption = '- Elemento'
    TabOrder = 9
    OnClick = btnBajaClick
  end
end

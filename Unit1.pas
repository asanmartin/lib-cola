unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls;

type
  TForm1 = class(TForm)
    lst1: TListBox;
    btnCrear: TButton;
    btnCheck: TButton;
    edtNombre: TEdit;
    edtRuta: TEdit;
    grp1: TGroupBox;
    lblNombre: TLabel;
    lblRuta: TLabel;
    btnEliminar: TButton;
    btnClear: TButton;
    btnAlta: TButton;
    grp2: TGroupBox;
    lblClave: TLabel;
    lblName: TLabel;
    edtClave: TEdit;
    edtName: TEdit;
    btnListado: TButton;
    btnBaja: TButton;
    procedure btnCheckClick(Sender: TObject);
    procedure btnCrearClick(Sender: TObject);
    procedure btnEliminarClick(Sender: TObject);
    procedure btnClearClick(Sender: TObject);
    procedure btnAltaClick(Sender: TObject);
    procedure btnListadoClick(Sender: TObject);
    procedure btnBajaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  LO_Cola;

var
  cola :tCola;

{$R *.dfm}

procedure TForm1.btnCheckClick(Sender: TObject);
var bExtCon,bExtDat:Boolean; ruta, nombre :string;
begin
  ruta := edtRuta.Text;
  nombre := edtNombre.Text;

  bExtCon := fileexists(ruta + nombre + '.CON');
  bExtDat := fileexists(ruta + nombre + '.DAT');
  if bExtCon then
    lst1.Items.Add('Existe Archivo de Control')
  else
    lst1.Items.Add('No Existe Archivo de Control');
  if bExtDat then
    lst1.Items.Add('Existe Archivo de Datos')
  else
    lst1.Items.Add('No Existe Archivo de Datos');
end;

procedure TForm1.btnCrearClick(Sender: TObject);
var ruta, nombre :string;
begin
  ruta := edtRuta.Text;
  nombre := edtNombre.Text;
  LO_CrearCola(cola, nombre, ruta);
end;

procedure TForm1.btnEliminarClick(Sender: TObject);
var ruta, nombre :string;
begin
  ruta := edtRuta.Text;
  nombre := edtNombre.Text;
  LO_DestruirCola(cola, nombre, ruta);
end;

procedure TForm1.btnClearClick(Sender: TObject);
begin
  lst1.Clear;
end;

function checkString(edt:string):Boolean;
begin
  //True = valida , False = esta vacia
  checkString := not(edt = '');
end;
function buscarEnCola(var cola:tCola; clave:tClave):Boolean;
var
  encontre:Boolean; i:Integer; RD :tRegistroDatos;
begin
  encontre := False;
  if not LO_ColaVacia(cola) then begin
    for i:= 1 to LO_TotalEnCola(cola) do begin
      LO_Frente(cola, RD);
      LO_Decolar(cola);
      if RD.clave = clave then encontre := True;
      LO_Encolar(cola, RD);
    end;
  end;
  buscarEnCola := encontre;
end;

procedure TForm1.btnAltaClick(Sender: TObject);
var
  RD: tRegistroDatos;
begin
  RD.clave := edtClave.Text;
  RD.nombre := edtName.Text;

  if checkString(RD.clave) then begin
    if checkString(RD.nombre) then begin
      //aca trabajamos la logica del alta
      LO_AbrirCola(cola);
      //faltaria hacer una busqueda por claves, para decir si se encuentra en la cola
      if buscarEnCola(cola, RD.clave) then
        lst1.Items.Add('La clave ingresada ya existe')
      else
        LO_Encolar(cola, RD);

      LO_CerrarCola(cola);
    end else
      lst1.Items.Add('El nombre esta vacio')
  end else
    lst1.Items.Add('La clave esta vacia');
end;

procedure TForm1.btnListadoClick(Sender: TObject);
var i:Integer; RD :tRegistroDatos;
begin
  btnClear.Click;
  LO_AbrirCola(cola);
  if not LO_ColaVacia(cola) then begin
    for i:= 1 to LO_TotalEnCola(cola) do begin
      LO_Frente(cola, RD);
      lst1.Items.Add(RD.nombre + '('+ RD.clave +')');
      
      LO_Decolar(cola);
      LO_Encolar(cola, RD);
    end;
  end else 
  lst1.Items.Add('Esta Vacio');
  LO_CerrarCola(cola);
end;

procedure TForm1.btnBajaClick(Sender: TObject);
var
  clave :string;
begin
  clave := edtClave.Text;

  if checkString(clave) then begin
    LO_AbrirCola(cola);
    if not buscarEnCola(cola, clave) then
      lst1.Items.Add('La clave ingresada no existe')
    else
      LO_Decolar(cola);
    //end-if
    LO_CerrarCola(cola);
  end else
    lst1.Items.Add('La clave esta vacia');
end;

end.
